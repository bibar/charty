/** Fixture **/

var cebada = {name:"cebada", data: [3148081, 2555652, 2589203, 3303404, 4222505, 5663006, 5255307, 7389708, 11189979, 160259010, 1148051]};
var maiz = {name:"maiz", data: [ 7955301, null, 8223003, 9196054, 11468705, 10412006, 11334007, 14281068, 15882959, 160658310, 1493005]};
var altmaiz = {name:"alt maiz", data: [ 7955301, null, 8223003, 9196054, 11468705, null, 11334007, 14281068, 15882959, 160658310, 1493005]};
var categorias = ["x", "03-04", "04-05", "05-06", "06-07", "07-08", "08-09", "09-10", "10-11", "11-12", "12-13", "13-14"];

var sample1 = [ cebada.name ];
cebada.data.map(function(d){ sample1.push(d) });
var sample2 = [ maiz.name ];
maiz.data.map(function(d){ sample2.push(d) });

var sample3 = [ altmaiz.name ];
altmaiz.data.map(function(f){ sample3.push(f) });

console.log( 'sample1: ', sample1 );
console.log( 'sample2: ', sample2 );
/** END Fixture **/

var el = document.getElementById('mychart');
var testCharty = Charty( el ); // create new charty passing it an ID *required*

testCharty.addTitle( 'Gráfico de Prueba' );
testCharty.setData( [sample1,sample2], 'columns' );
testCharty.setCategories( categorias );

var formatFn = function (d) { return d + " has"; };
testCharty.addLabelY( 'some name', formatFn );

testCharty.setAlternateChart( [categorias, sample3, sample1], 'columns', 'Alt Maiz', 'Corn Label' );
testCharty.addAlternateButtons();testCharty.addAlternateButtons();testCharty.addAlternateButtons();

testCharty.addCheckboxes();

testCharty.regenerate();

setTimeout(function(){
  testCharty.enableFilterX( [1, 11] );
  testCharty.regenerate();
}, 1500)

setTimeout(function(){
  testCharty.disableFilterX( [1, 11] );
  testCharty.regenerate();
}, 3000)

