var gulp = require('gulp');
var gutil = require('gulp-util');
var webpack = require('webpack');
var browserSync = require('browser-sync');
var webpackConfig = require('./webpack.config');
var clean = require('gulp-clean');

/** Clean Task **/
gulp.task('clean', function ()  {
  return gulp.src('dist', {read: false})
        .pipe(clean());
});
/** End Clean Task */

/** Begin Production Build **/
gulp.task('build', ['webpack:build']);

gulp.task('webpack:build', ['clean'], function(callback) {
  // modify some webpack config options
  var myConfig = Object.create(webpackConfig);
  myConfig.plugins = myConfig.plugins.concat(
    new webpack.DefinePlugin({
      "process.env": {
        // This has effect on the react lib size
        "NODE_ENV": JSON.stringify("production")
      }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.UglifyJsPlugin()
  );

  // run webpack
  webpack(myConfig, function(err, stats) {
    if(err) throw new gutil.PluginError('webpack:build', err);
    gutil.log('[webpack:build]', stats.toString({
      colors: true
    }));
    callback();
  });
});
/** End Production Build **/

/** Begin Development Build **/

// modify some webpack config options
var myDevConfig = Object.create(webpackConfig);
myDevConfig.devtool = 'sourcemap';
myDevConfig.debug = true;
myDevConfig.cache = false;
// use the component as a global library to showcase
myDevConfig.output.library = 'Charty';

// create a single instance of the compiler to allow caching
var devCompiler = webpack(myDevConfig);

gulp.task('dev', ['webpack:build-dev']);

gulp.task('webpack:build-dev', function(callback) {
  // run webpack
  devCompiler.run(function(err, stats) {
    if(err) throw new gutil.PluginError('webpack:build-dev', err);
    gutil.log('[webpack:build-dev]', stats.toString({
      colors: true
    }));
    callback();
  });
});
/** End Development Build **/

/* Live Development Server for showcase */
gulp.task('browser-sync', ['dev'], function() {
    browserSync({
        server: {
            baseDir: ['showcase', 'dist']
        }
    });
});

gulp.task('serve', ['browser-sync'], function () {

  gulp.watch(['index.js', 'index.css', 'showcase/**/*', 'custom_modules/**/*'], ['dev', browserSync.reload]);

});

gulp.task('default', ['serve']);