'use strict';
/** Module deps **/
var c3 = require( 'c3' );
require( 'c3/c3.css' ).use();
//var tpl = require( 'html!./index.html' ); // don't know why this does not work
require( './index.css' ).use();

/** Module vars **/
var DEFAULT_LABEL = {text:'', position: 'outer-middle'};

/** Module exports **/
module.exports = Charty;

/** Module constructor **/
function Charty( elID, options ){
  if ( 'undefined' === typeof this ){
    return new Charty( elID, options );
  }

  if ( 'undefined' === typeof elID ){
    throw new Error( 'An id string or DOM Node is required.' );
  }

  var el;

  if ( 'string' === typeof elID ){
    this.moduleID = elID;
    // insert dynamic ids to template stuff
    el = document.getElementById( this.moduleID );
  }else{
    this.moduleID = elID.id || 'charty_module';
    el = elID;
  }

  this.chartID = this.moduleID + '_chart';

  if ( !el ){
    el = document.createElement( 'section' );
    el.id = this.moduleID;
    document.body.appendChild( el );
  }

  el.classList.add( 'charty' );

  var chartyEl = el.querySelector( 'div' );
  if ( !chartyEl ){
    chartyEl = document.createElement( 'div' );
  }
  chartyEl.id = this.chartID;
  chartyEl.classList.add( 'charty' );

  el.appendChild( chartyEl );
  this.parentEl = el;
  // create instance object
  this._instance = {};
  // declare some _private_ variables
  this._instance.bindto = '#' + this.chartID;
  this._instance.data = {};
  this._instance.axis = {};

  this._alternateData = []; // array of data for new objects instances // TODO: rename it to something like _chartsData
  this._currentChartIDX = 0; // pointer to current chart data.
  this._alternateData[ 0 ] = {};
  this._originalTitle = 'Default Chart';
  this._originalCategories = [];
  this._categories = [];
  this._s = new Set();
  // prepare options
  if ( options ){
    this.prepareOptions( options );
  }

  this._namespace

  return this;
}

/** Module methods **/

Charty.prototype.regenerate = function(){
  console.log( 'c3 instance object: ', this._instance );
  // create initial c3 instance
  this.newCharty = c3.generate( this._instance );
}

Charty.prototype.prepareOptions = function( options ){
  /** Sample options object
  {
    bindto: '#' + this.chartID,
    data: {
      format : 'rows' | 'columns' | 'json',
      data: [...]
    },
    type: someChartType, // default is empty, which is line chart.
    labelx: someLabel,
    labely: someLabel,
    categories: [...] // array of strings representing x axis categories // this could be already defined on top of data
  }
  **/

  if ( 'object' !== typeof options ){
    throw new Error( 'An object is required.' );
  }

  this.YLabel = options.dataY || DEFAULT_LABEL;
  this.XLabel = options.dataX || DEFAULT_LABEL;


  if ( options.type ){
    this.setType( options.type );
  }

  if ( options.title ){
    this.addTitle( options.title );
  }

  if ( options.labelx ){
    this.addLabelX( options.labelx );
  }

  if ( options.labely ){
    this.addLabelY( options.labely );
  }

  if ( options.categories ){
    this.setCategories( options.categories );
  }

  if ( options.data ){
    this.setData( options.data.format, options.data.data, true );
  }

  return this.regenerate();
}

/**
 * setCategories Allows the user to set custom values on the x axis.
 * @param {Array} categories An array of values representing each one a label.
 */
Charty.prototype.setCategories = function( categories ){
  if ( 'undefined' === typeof this._instance.axis.x ){
    this._instance.axis.x = {};
  }
  this._instance.data.x = 'x';
  this._instance.axis.x.type = 'category';

  if ( !categories ){
    return;
  }
  if ( this._originalCategories.length == 0 ){
    this._originalCategories = categories;
    this._alternateData[ 0 ].categories = this._originalCategories;
  }
  this._categories = categories;
  var _temp = [ categories ];
  var currentData = this._alternateData[ 0 ].data || [];
  currentData.map(function(d){
    _temp.push( d );
  })
  this._alternateData[ 0 ].data = _temp;

  this.setData( this._alternateData[ 0 ].data, this.dataFormat );

  return this;
}

Charty.prototype.setType = function( chartType ){
  if ( 'string' !== typeof chartType ){
    throw new Error( 'A string is required' );
  }
  this._instance.data.type = chartType;
}

/**
 * setData Sets data for default chart. Equivalent to create Charty with data sets in options.
 * @param {[type]} data   [description]
 * @param {[type]} format [description]
 */
Charty.prototype.setData = function( data, format ){

  if ( 'undefined' === typeof data ){
    throw new Error( 'Some data is required.' );
  }
  format = format || 'columns'; // formats: columns | rows | json

  // DEPRECATED
  if ( 'undefined' === typeof this._originalData ){
    this._originalData = data;
  }

  // DEPRECATED
  if ( format ){
    this.dataFormat = format;
  }

  if ( this._alternateData.length == 0 || 'undefined' === typeof this._alternateData.data ){
    this._alternateData[ 0 ] = {data: data, format: this.dataFormat, title: this._originalTitle, categories: data[0], _originalData: data};
  }

  this._setInstanceData( 0 );

  return this;
}

Charty.prototype.setAlternateChart = function( data, format, title, labely ){
  if ( 'undefined' === typeof data ){
    throw new Error( 'Some data is required.' );
  }
  format = format || 'columns'; // formats: columns | rows | json
  title = title || '';
  labely = labely || '';
  var found = false;

  for ( var i=0; i<this._alternateData.length; i++ ){
    var value = this._alternateData[ i ];
    if ( value.title === title ){
      found = true;
      value = null;
      break;
    }
  }
  if ( !found ){
    this._alternateData.push( {data: data, format: format, title: title, categories: data[0], labely: labely, _originalData: data} );
  }

  return this;
}


/**
 * _setInstanceData Overwrite internal instance object (IIO) with data at idx.
 * @param {Number} idx A number representing an index.
 * @private
 */
Charty.prototype._setInstanceData = function( idx ){
  if ( 'undefined' === typeof this._instance.data ){
    this._instance.data = {};
  }
  var currentData = this._alternateData[ idx ];

  if ( 'object' === typeof currentData ){
    this._instance.data[ currentData.format ] = currentData.data;
  }

}

Charty.prototype.enableFilterX = function( idx ){
  if ( 'undefined' === typeof idx ){
    throw new Error( 'A number or array is required.' );
  }

  var data = this._alternateData[ this._currentChartIDX ];

  if ( 'number' === typeof idx ){
    var _tmp = idx;
    idx = [ _tmp ];
    _tmp = null;
  } else if ( !Array.isArray(idx) ){
    throw new Error( 'A number or array is required.' );
  }

  idx.map(function(e){ this._s.add( e ) }, this);

  this._filterX();
}

Charty.prototype._filterX = function(){
  var data = this._copy( this._alternateData[ this._currentChartIDX ]._originalData );

  data.map(function( d ){
    var self = this;
    data[ data.indexOf(d) ] = d.filter(function(value, valIdx, arr){
      return ( this._s.has(valIdx) ) ? false : true;
    }, self)
  }, this);

  this._alternateData[ this._currentChartIDX ].data = data;
  this._setInstanceData( this._currentChartIDX );
}

Charty.prototype._copy = function( arr ){
  var cpy = arr.map(function(d){
    return d;
  })
  return cpy;
}

Charty.prototype.disableFilterX = function( idx ){
  if ( 'undefined' === typeof idx ){
    throw new Error( 'A number or array is required.' );
  }

  if ( 'number' === typeof idx ){
    var _tmp = idx;
    idx = [ _tmp ];
    _tmp = null;
  } else if ( !Array.isArray(idx) ){
    throw new Error( 'A number or array is required.' );
  }

  idx.map(function(e){ this._s.delete( e ) }, this);

  this._filterX();
}

Charty.prototype.loadAlternateData = function( ev ){
  if ( !ev ){
    return;
  }
  var target = ev.currentTarget;
  var value = Number( target.value );
  this._currentChartIDX = value;
  var alternateData = this._alternateData[ value ];
  if ( !alternateData ){
    return;
  }
  // this should be like a re-setup
  this.removeCheckboxes();
  alternateData.data = this._copy( alternateData._originalData ); // restore original data
  this._setInstanceData( value );
  console.log( 'loadAlternateData: ', this._instance );
  alternateData.categories = alternateData.data[ 0 ];
  this.addTitle( alternateData.title );
  if ( alternateData.labely ){
    this.addLabelY( alternateData.labely );
  }
  this.addCheckboxes(); // TODO: check if user has setted checkboxes before add them.
  ev.stopPropagation();

  this.regenerate();
}

Charty.prototype._restoreOriginal = function(){
  this.addTitle( this._originalData );
  this._setInstanceData( this._originalData, this.dataFormat );
  this.regenerate();
}

Charty.prototype.toggleFilter = function( ev ){
  if ( !ev ){
    return;
  }
  var target = ev.currentTarget;
  var value = Number( target.value );

  if ( target.checked ){

    console.log( 'disabling filtering...' );

    this.disableFilterX( value );

  }else{

    console.log( 'enabling filtering...' );

    this.enableFilterX( value );
  }

  this.regenerate();
}

Charty.prototype.removeCheckboxes = function(){
  var el = this.parentEl.querySelector( '.filter_container' );
  if ( el ){
    var parent = el.parentElement;
    parent.removeChild( el );
    this._s = new Set(); // reset filter set
    el = null;
  }
  return this;
}

Charty.prototype.addCheckboxes = function(){

  var el = this.parentEl;
  var checks = [];
  var check, icontent, checkBtn, checkWrp, container, label, categories;

  container = this.parentEl.querySelector( '.filter_container' );

  if ( container && container.parentNode ){
    container.parentNode.removeChild( container );
  }
  container = document.createElement( 'div' );
  container.classList.add( 'filter_container', 'row' );

  categories = this._alternateData[ this._currentChartIDX ].categories;

  if ( !categories ){
    return;
  }

  for ( var i = 1; i < categories.length; i++ ){
    var elID = categories[ i ];
    checkWrp = document.createElement( 'div' );
    checkWrp.classList.add( 'wrap' );
    checkBtn = document.createElement( 'div' );
    checkBtn.classList.add( 'chartCheckBtn' );
    icontent = document.createElement( 'i' );
    icontent.appendChild( document.createTextNode('✔') );

    check = document.createElement( 'input' );
    label = document.createElement( 'label' );
    label.appendChild( document.createTextNode(elID) );
    label.htmlFor = elID;
    check.type = 'checkbox';
    check.checked = true;
    check.id = elID;
    check.label = elID;
    check.name = elID;
    check.value = i;
    check.addEventListener( 'click', this.toggleFilter.bind(this), false );

    checkBtn.appendChild( check );
    checkBtn.appendChild( icontent );
    checkBtn.appendChild( label );
    checkWrp.appendChild( checkBtn );
    container.appendChild( checkWrp );
  }

  el.appendChild( container );
}

Charty.prototype.addAlternateButtons = function(){
  var el = this.parentEl;
  var buttons = [];
  var button, container;

  if ( !this._alternateData.length ){
    return;
  }

  container = this.parentEl.querySelector( '.buttons_container' );
  if ( container && container.parentNode ){
    container.parentNode.removeChild( container );
  }
  container = document.createElement( 'div' );
  container.classList.add( 'buttons_container', 'row' );

  for ( var i = 0; i < this._alternateData.length; i++ ){
    var item = this._alternateData[ i ];
    button = document.createElement( 'button' );
    button.type = 'button';
    button.classList.add( 'charty_button', 'center', 'alternateBtn', 'small', 'alternateBtn-orange', 'col-xs-4', 'col-md-4' );
    button.appendChild( document.createTextNode(item.title) );
    button.value = i;
    button.addEventListener( 'click', this.loadAlternateData.bind(this), false );
    container.appendChild( button );
  }
  el.appendChild( container );

}

Charty.prototype.addTitle = function( aTitle ){
  if ( 'undefined' === typeof aTitle ){
    throw new Error( 'A title is required.' );
  }
  if ( 'string' !== typeof aTitle ){
    throw new Error( 'A string is required.' );
  }

  var chart = document.getElementById( this.chartID );

  if ( chart ){
    var h3 = this.parentEl.querySelector( 'h3' );
    if ( !h3 ){
      h3 = document.createElement( 'h3' );
      this._originalTitle = aTitle;
    }
    h3.textContent = aTitle;
    chart.parentElement.insertBefore( h3, chart );
    h3 = null;
  }
  chart = null;
}

/**
 * addLabelX sets a new label on axis X
 * @param {String} A new x label.
 * @param {Function} format A label formatter
 */
Charty.prototype.addLabelX = function( aLabel, format ){

  if ( 'undefined' === typeof aLabel ){
    throw new Error( 'A label is required.' );
  }

  if ( 'string' !== typeof aLabel ){
    throw new Error( 'A label string is required.' );
  }

  this._instance.axis.x = this._instance.axis.x || {};
  this._instance.axis.x.label = aLabel;
  if ( format ){
    this._instance.axis.x.tick = {};
    this._instance.axis.x.tick.format = format;
  }
  return this; // its cool to chain methods
}

/**
 * addLabelY sets a new label on axis Y
 * @param {String} A new y label.
 * @param {Function} format A label formatter
 */
Charty.prototype.addLabelY = function( aLabel, format ){

  if ( 'undefined' === typeof aLabel ){
    throw new Error( 'A label is required.' );
  }

  if ( 'string' !== typeof aLabel ){
    throw new Error( 'A label string is required.' );
  }

  this._instance.axis.y = this._instance.axis.y || {};
  this._instance.axis.y.label = aLabel;

  if ( ('undefined' === typeof this._alternateData[ 0 ].labely) || (this._alternateData[ 0 ].labely.length == 0) ){
    this._alternateData[ 0 ].labely = aLabel;
  }

  if ( format ){
    this._instance.axis.y.tick = {};
    this._instance.axis.y.tick.format = format;
  }

  return this; // its cool to chain methods
}