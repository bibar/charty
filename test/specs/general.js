/** Fixture **/

/*var cebada = {name:"cebada", data: [3148081, 2555652, 2589203, 3303404, 4222505, 5663006, 5255307, 7389708, 11189979, 160259010, 1148051]};
var maiz = {name:"maiz", data: [ 7955301, null, 8223003, 9196054, 11468705, 10412006, 11334007, 14281068, 15882959, 160658310, 1493005]};
var categorias = ["x", "03-04", "04-05", "05-06", "06-07", "07-08", "08-09", "09-10", "10-11", "11-12", "12-13", "13-14"];

var d1 = [ cebada.name ];
cebada.data.map(function(d){ d1.push(d) });
var d2 = [ maiz.name ];
maiz.data.map(function(d){ d2.push(d) });

console.log( 'd1: ', d1 );
console.log( 'd2: ', d2 );*/
/** END Fixture **/

QUnit.module("API General");

QUnit.test("Global functions", function(assert) {
  expect(2);

  assert.equal(typeof Charty, "function", "Defines a global Charty function");
  assert.equal(typeof window.Charty, "function", "Defines a global Charty function");

});

QUnit.test("Construction", function(assert) {

  var testCharty = new Charty( 'mychart' ); // create new charty passing it an ID *required*

  //var newEl = document.body.querySelector('.charty')[0];
  expect(1);

  assert.equal( typeof testCharty, "object", "Defines a simple Charty instance" );
  //assert.equal( typeof newEl, "object", "Defines a new element containing the chart created by the module" );

});
/*
var testCharty = new Charty( 'mychart' ); // create new charty passing it an ID *required*

testCharty.setData( [d1,d2], 'columns' );
testCharty.setCategories( categorias );

testCharty.addTitle( 'Gráfico de Prueba' );
var formatFn = function (d) { return d + " has"; };
testCharty.addLabelY( 'some name', formatFn );
testCharty.regenerate();

*/